// console.log("Hello World");



// ------------------------------------
function toRaise(number){
    const getCube = Math.pow(number, 3);
    
    console.log(`The cube of ${number} is ${getCube}.`);
}
toRaise(2);





// -------------------------------------
const address = ["258 Washington Ave","NW", "California 90011"];

const [streetName, building, state] = address;
console.log(`I live at ${streetName} ${building}, ${state}.`);






// ----------------------------------------
const animal = {
    name: "Lolong",
    type: "saltwater crocodile",
    weight: 1075,
    measurement: "20 ft 3 in"
};

const {name, type, weight, measurement} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${measurement}.`);





// ---------------------------------------
const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
    console.log(number)
});





// ---------------------------------------
const initialValue = 0;
const reduceNumber = numbers.reduce((previousValue, currentValue) => previousValue + currentValue, initialValue);

console.log(reduceNumber);






// ----------------------------------------
class Dog {
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
};

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);